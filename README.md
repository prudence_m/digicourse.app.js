## Table of Contents

- [Resources](./docs/RESOURCE.md)
- [Development](./docs/DEVELOPMENT.md)
- [Architecture](./docs/ARCHITECTURE.md)
