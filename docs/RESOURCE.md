## Resources

Additional reading material.

### Frameworks

- [NextJS](https://github.com/unicodeveloper/awesome-nextjs)
- [React](https://github.com/enaqx/awesome-react)

### Libraries

- [Next Routes](https://github.com/fridays/next-routes)
- [Next Auth](https://github.com/iaincollins/next-auth)

### Project Specific Versions

- [NextJS v5.1.0](https://github.com/zeit/next.js/tree/5.1.0)
