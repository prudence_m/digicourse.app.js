## Project Diagram

```

^
├── [~/.repo/digicourse.app.js]
^
|  /*
|   *   APP_SCOPE 
|   *
|   *   multi-entrypoints based on filesystem 
|   *
|   *   http://localhost:3000/
|   *   http://localhost:3000/dashboard
|   *
|   */
|
├── [pages]
^   ^
|   |   index.js
|   |   dashboard.js
|   |   login.js
|   |   register.js
|   |   packages.js
|   |
|   ...
|
├── [src]
^   ^
|   |  /*
|   |   *   SRC_SCOPE
|   |   *
|   |   *   each entrypoint on startup
|   |   *
|   |   */
|   |
|   ├── [containers]
|   ^   ^
|   |   |  /*
|   |   |   *   CONTAINER_SCOPE 
|   |   |   *
|   |   |   *   each entrypoint must
|   |   |   *   connect to redux store
|   |   |   *   get it's actions
|   |   |   *   render a page component 
|   |   |   *
|   |   |   */
|   |   |
|   |   ├── [home]
|   |   ^   ^
|   |   |   |   index.js
|   |   |   |   index.test.js
|   |   |   |
|   |   |   ...
|   |   |
|   |   ├── [dashboard]
|   |   ^   ^
|   |   |   |   index.js
|   |   |   |   index.test.js
|   |   |   |
|   |   |   ...
|   |   |
|   |   ├── [login]
|   |   ^   ^
|   |   |   |   index.js
|   |   |   |   index.test.js
|   |   |   |
|   |   |   ...
|   |   |
|   |   ├── [register]
|   |   ^   ^
|   |   |   |   index.js
|   |   |   |   index.test.js
|   |   |   |
|   |   |   ...
|   |   |
|   |	├── [packages]
|   |	^   ^
|   |   |   |   index.js
|   |   |   |   index.test.js
|   |   |   ...
|   |   |	
|   |	|
|   ├── [components]
|   ^   ^
|   |   |  /*  
|   |   |   *   COMPONENT_SCOPE
|   |   |   *
|   |   |   *   a re-useable block of code
|   |   |   *   takes in props
|   |   |   *   asserts against props
|   |   |   *   made up of parts and other components
|   |   |   *
|   |   |   */
|   |   |
|   |   ├── [page]
|   |   ^   ^
|   |   |   |   index.js
|   |   |   |   index.test.js
|   |   |   |
|   |   |   ...
|   |   |
|   |   ├── [login]
|   |   ^   ^
|   |   |   |   index.js
|   |   |   |   index.test.js
|   |   |   |
|   |   |   ...
|   |   |
|   |   ├── [register]
|   |   ^   ^
|   |   |   |  index.js
|   |   |   |  index.test.js
|   |   |   |
|   |   |   ...
|   |   |
|   |   ├── [packages]
|   |   ^   ^
|   |   |   |  index.js
|   |   |   |  index.test.js
|   |   |   |
|   |   |   ...
|   |   |
|   |   ...
|   |
|   ├── [parts]
|   ^   ^
|   |   |  /*  
|   |   |   *   PART_SCOPE
|   |   |   *
|   |   |   *   stateless elements
|   |   |   *
|   |   |   */
|   |   |
|   |   ├── [Button]
|   |   ^   ^
|   |   |   |   index.js
|   |   |   |   index.test.js
|   |   |   |
|   |   |   ...
|   |   |
|   |   ├── [Menu]
|   |   ^   ^
|   |   |   |   index.js
|   |   |   |   index.test.js
|   |   |   |
|   |   |   ...
|   |   |
|   |   ├── [AppBar]
|   |   ^   ^
|   |   |   |   index.js
|   |   |   |   index.test.js
|   |   |   |
|   |   |   ...
|   |   |
|   |   ...
|   |
|   ...
^
├── [server]
^   ^
|   |  /*
|   |   *   SERVER_SCOPE
|   |   *
|   |   *   listens for incoming requests
|   |   *
|   |   */
|   |
|   |   index.js
|   |   index.test.js
|   |
|   ├── [routes]
|   ^	^
|   |   |   /*
|   |   |    *   ROUTE_SCOPE
|   |   |    *
|   |   |    *   mapped to the pages dir
|   |   |    *
|   |   |    */
|   |   |
|   |   |   index.js
|   |   |   index.test.js
|   |   |
|   |   ...
|   |
|   ├── [logger]
|   ^   ^
|   |   |  /*
|   |   |   *   LOGGER_SCOPE
|   |   |   *
|   |   |   *   used to emit logs in JSON format
|   |   |   *
|   |   |   */
|   |   | 
|   |   |   index.js
|   |   |   index.test.js
|   |   |
|   |   ...
|   |
|   ...
|
└── app.config.js
```

