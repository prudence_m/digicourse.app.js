## Architecture

Part is the lowest building block for the web.

- [Project Diagram](./PROJECT_DIAGRAM.md)

### Running

In order to run the project with complete architecture setup.

```bash
make build
make run
```
