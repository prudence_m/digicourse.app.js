import Link from 'next/link'
import Head from '../src/components/head'
import Nav from '../src/components/nav'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import Checkbox from 'material-ui/Checkbox';
import {grey500, white} from 'material-ui/styles/colors';
import PersonAdd from 'material-ui/svg-icons/social/person-add';
import Help from 'material-ui/svg-icons/action/help';
import TextField from 'material-ui/TextField';

const LoginPage = () => {

   const styles = {
     loginContainer: {
       minWidth: 920,
       maxWidth: 400,
       height: 'auto',
       position: 'absolute',
      top: '20%',
      left: 0,
       right: 0,
       margin: 'auto'
     },
    paper: {
       padding: 20,
       overflow: 'auto'
     },
    buttonsDiv: {
       textAlign: 'center',
       padding: 10
     },
     flatButton: {
       color: grey500
     },
   
       labelStyle: {
        color: grey500
      },
      iconStyle: {
         color: grey500,
        borderColor: grey500,
         fill: grey500
      }
    };
   
   

   return (
     <MuiThemeProvider >
       <div>
         <div style={styles.loginContainer}>

           <Paper style={styles.paper}>

 <form onSubmit={this.handleSubmit} style={styles.form}>
   <h2 style={styles.h2}>
          ID Required
        </h2>
        <p style={styles.p}>
          Please fill out the form below.
        </p>
        <TextField
          name='email'
          autoFocus
          floatingLabelText='Email'
          type='email'
          floatingLabelFixed
        />
        <br />
        <TextField
          name='password'
          floatingLabelText='Password'
          type='password'
          floatingLabelFixed
        />
        <br />
        <div>
          <p style={styles.p}>
            <a href='./register'>Register</a>
            &nbsp;
            |
            &nbsp;
            <a href='./user/reset'>Reset Password</a>
            &nbsp;
            |
            &nbsp;
            <a href='./static/terms.md'>Terms</a>
          </p>
        </div>
        <br />
        <div style={styles.div}>
          <RaisedButton style={styles.btn} label='Submit' onClick={e => handleSubmit(e)} primary />
        </div>
      </form>

           </Paper>

          <div style={styles.buttonsDiv}>
            
          </div>

        </div>
       </div>
     </MuiThemeProvider>
   );
 };

 export default LoginPage;
