import Head from '../src/components/head'
import Nav from '../src/components/nav'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import Checkbox from 'material-ui/Checkbox';
import {grey500, white} from 'material-ui/styles/colors';
import PersonAdd from 'material-ui/svg-icons/social/person-add';
import Help from 'material-ui/svg-icons/action/help';
import TextField from 'material-ui/TextField';
import AppBar from 'material-ui/AppBar';
import MenuItem from 'material-ui/MenuItem';
import { Link } from '../server/routes';

const LoginPage = () => {

   const styles = {
     loginContainer: {
       minWidth: 920,
       maxWidth: 400,
       height: 'auto',
       position: 'absolute',
      top: '20%',
      left: 0,
       right: 0,
       margin: 'auto'
     },
    paper: {
       padding: 20,
       overflow: 'auto'
     },
    buttonsDiv: {
       textAlign: 'center',
       padding: 10
     },
     flatButton: {
       color: grey500
     },

       labelStyle: {
        color: grey500
      },
      iconStyle: {
         color: grey500,
        borderColor: grey500,
         fill: grey500
      }
    };



   return (
     <MuiThemeProvider >
       <div>
         <AppBar
             label="Open Drawer"
             onClick={this.handleToggle}>

  <MenuItem onClick={this.handleClose}>
             <Link href="/register"><a style={styles.navButton}>Register</a></Link>
         </MenuItem>

         <MenuItem onClick={this.handleClose}>
             <Link href="/login"><a style={styles.navButton}>Login</a></Link>
         </MenuItem>
     </AppBar>






         <div style={styles.loginContainer}>









          <Paper style={styles.paper}>

            <form>

              <b>What is DigiCourse</b>
                  DigiCourse is a convenient online course management web application which is specially designed and developed to assist with the management offered courses. DigiCourse also offers enrolment of IT related courses to further the skills of programmers or individuals looking to learn programming with a variety of programming languages to choose from.

                <h1> </h1>
              <b>What do we offer</b>

                 DigiCourse offers a variety of learning packages with various benefits provided to assist novice developers and proffesionals to upscale their knowledge and development skills in the world of IT, we pride ourselves in providing access to tutors and up to date study material with interactive study sessions depending on the learning package chosen by you…
                 <h1></h1>
                 <b>How to Register</b>
                  Start now and register for our courses on our fast and easy registration process with 4 quick hassle-free steps.
                  <h1></h1>
                 <Link to="/">

                  <RaisedButton label="Proceed"
                                primary={true}
                                style={styles.loginBtn}/>
                </Link>
              </form>
            </Paper>

          <div style={styles.buttonsDiv}>

          </div>

        </div>
       </div>
     </MuiThemeProvider>
   );
 };

 export default LoginPage;
