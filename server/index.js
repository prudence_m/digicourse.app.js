/*const next = require('next')
const routes = require('./routes')
const app = next({dev: process.env.NODE_ENV !== 'production'})
const handler = routes.getRequestHandler(app)

// With express
const express = require('express')
app.prepare().then(() => {
  express().use(handler).listen(3000)
})*/

const next = require('next');
const express = require('express');
const routes = require('./routes');
//const interceptor = require('./interceptor/index.js');
//const Database = require('./database');
//const log = require('./logger')({ name: __filename });
//const config = require('../app.config');

const port = parseInt(process.env.PORT, 10) || 3000;
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handler = routes.getRequestHandler(app);

//const db = new Database(config);
//log.info({ db });
 
app
  .prepare()
  .then(() => {
    express()
      .use(handler)
      .listen(port);
  });
