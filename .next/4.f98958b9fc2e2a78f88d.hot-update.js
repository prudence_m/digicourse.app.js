webpackHotUpdate(4,{

/***/ "./pages/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("./node_modules/react/cjs/react.development.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_next_link__ = __webpack_require__("./node_modules/next/link.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_next_link___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_next_link__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__src_components_head__ = __webpack_require__("./src/components/head.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__src_components_nav__ = __webpack_require__("./src/components/nav.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_material_ui_styles_MuiThemeProvider__ = __webpack_require__("./node_modules/material-ui/styles/MuiThemeProvider.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_material_ui_styles_MuiThemeProvider___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_material_ui_styles_MuiThemeProvider__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_material_ui_Paper__ = __webpack_require__("./node_modules/material-ui/Paper/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_material_ui_Paper___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_material_ui_Paper__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_material_ui_RaisedButton__ = __webpack_require__("./node_modules/material-ui/RaisedButton/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_material_ui_RaisedButton___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_material_ui_RaisedButton__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_material_ui_FlatButton__ = __webpack_require__("./node_modules/material-ui/FlatButton/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_material_ui_FlatButton___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_material_ui_FlatButton__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_material_ui_Checkbox__ = __webpack_require__("./node_modules/material-ui/Checkbox/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_material_ui_Checkbox___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_material_ui_Checkbox__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_material_ui_styles_colors__ = __webpack_require__("./node_modules/material-ui/styles/colors.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_material_ui_styles_colors___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_material_ui_styles_colors__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_material_ui_svg_icons_social_person_add__ = __webpack_require__("./node_modules/material-ui/svg-icons/social/person-add.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_material_ui_svg_icons_social_person_add___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_material_ui_svg_icons_social_person_add__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_material_ui_svg_icons_action_help__ = __webpack_require__("./node_modules/material-ui/svg-icons/action/help.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_material_ui_svg_icons_action_help___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_material_ui_svg_icons_action_help__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_material_ui_TextField__ = __webpack_require__("./node_modules/material-ui/TextField/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_material_ui_TextField___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_12_material_ui_TextField__);
var _jsxFileName = "/Users/academy_intern1/repo/digicourse.app.js/pages/index.js";


(function () {
  var enterModule = __webpack_require__("./node_modules/react-hot-loader/index.js").enterModule;

  enterModule && enterModule(module);
})();














var LoginPage = function LoginPage() {
  var styles = {
    loginContainer: {
      minWidth: 920,
      maxWidth: 400,
      height: 'auto',
      position: 'absolute',
      top: '20%',
      left: 0,
      right: 0,
      margin: 'auto'
    },
    paper: {
      padding: 20,
      overflow: 'auto'
    },
    buttonsDiv: {
      textAlign: 'center',
      padding: 10
    },
    flatButton: {
      color: __WEBPACK_IMPORTED_MODULE_9_material_ui_styles_colors__["grey500"]
    },
    labelStyle: {
      color: __WEBPACK_IMPORTED_MODULE_9_material_ui_styles_colors__["grey500"]
    },
    iconStyle: {
      color: __WEBPACK_IMPORTED_MODULE_9_material_ui_styles_colors__["grey500"],
      borderColor: __WEBPACK_IMPORTED_MODULE_9_material_ui_styles_colors__["grey500"],
      fill: __WEBPACK_IMPORTED_MODULE_9_material_ui_styles_colors__["grey500"]
    }
  };
  return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_4_material_ui_styles_MuiThemeProvider___default.a, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 53
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 54
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
    style: styles.loginContainer,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 55
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_material_ui_Paper___default.a, {
    style: styles.paper,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 57
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("form", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 59
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("b", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 61
    }
  }, "What is DigiCourse"), "DigiCourse is a convenient online course management web application which is specially designed and developed to assist with the management offered courses. DigiCourse also offers enrolment of IT related courses to further the skills of programmers or individuals looking to learn programming with a variety of programming languages to choose from.", __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("h1", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 64
    }
  }, " "), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("b", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 65
    }
  }, "What do we offer"), "DigiCourse offers a variety of learning packages with various benefits provided to assist novice developers and proffesionals to upscale their knowledge and development skills in the world of IT, we pride ourselves in providing access to tutors and up to date study material with interactive study sessions depending on the learning package chosen by you\u2026", __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("h1", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 68
    }
  }), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("b", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 69
    }
  }, "How to Register"), "Start now and register for our courses on our fast and easy registration process with 4 quick hassle-free steps.", __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("h1", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 71
    }
  }), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_1_next_link___default.a, {
    to: "/",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 72
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_material_ui_RaisedButton___default.a, {
    label: "Proceed",
    primary: true,
    style: styles.loginBtn,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 74
    }
  })))), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
    style: styles.buttonsDiv,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 81
    }
  }))));
};

var _default = LoginPage;
/* harmony default export */ __webpack_exports__["default"] = (_default);
;

(function () {
  var reactHotLoader = __webpack_require__("./node_modules/react-hot-loader/index.js").default;

  var leaveModule = __webpack_require__("./node_modules/react-hot-loader/index.js").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(LoginPage, "LoginPage", "/Users/academy_intern1/repo/digicourse.app.js/pages/index.js");
  reactHotLoader.register(_default, "default", "/Users/academy_intern1/repo/digicourse.app.js/pages/index.js");
  leaveModule(module);
})();

;
    (function (Component, route) {
      if(!Component) return
      if (false) return
      module.hot.accept()
      Component.__route = route

      if (module.hot.status() === 'idle') return

      var components = next.router.components
      for (var r in components) {
        if (!components.hasOwnProperty(r)) continue

        if (components[r].Component.__route === route) {
          next.router.update(r, Component)
        }
      }
    })(typeof __webpack_exports__ !== 'undefined' ? __webpack_exports__.default : (module.exports.default || module.exports), "/")
  
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__("./node_modules/next/node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=4.f98958b9fc2e2a78f88d.hot-update.js.map